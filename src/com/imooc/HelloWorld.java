package com.imooc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class HelloWorld {
	//完成 main 方法
    @SuppressWarnings("null")
	public static void main(String[] args) throws IOException {
    	BufferedReader file = new BufferedReader(new FileReader(new File("F://QQ文档/591195690/FileRecv/学习/trainingset.txt")));
    	int[][] tranx=new int[35][2];
    	int[] trany=new int[35];
    	String rowTran;
    	int iTran = 0;
    	String[] tranSet = null;
    	while ((rowTran=file.readLine())!=null){
    		tranSet = rowTran.split(",");
        	for (int j=0;j<2;j++){
        		tranx[iTran][j]=Integer.parseInt(tranSet[j]);
        		}
        	trany[iTran]=Integer.parseInt(tranSet[2]);
        	iTran++;
    	}
    	iTran--;
//    	调用方法normalize 使x正则化并加上一列x0=1
    	HelloWorld normal=new HelloWorld();
    	double[][] tranx_ = normal.normalize(iTran, tranx);
//    	用梯度下降法求theta0,theta1,theta2
    	double theta0=0,theta1=0,theta2=0;
    	double temp0,temp1,temp2;
    	double alpha=0.01;
    	HelloWorld derivation=new HelloWorld();
    	for (int i=1;i<=400;i++){
    		temp0=theta0-alpha*derivation.derivate(theta0,theta1,theta2,iTran,0,trany,tranx_);
    		temp1=theta1-alpha*derivation.derivate(theta0,theta1,theta2,iTran,1,trany,tranx_);
    		temp2=theta2-alpha*derivation.derivate(theta0,theta1,theta2,iTran,2,trany,tranx_);
    		theta0=temp0;
    		theta1=temp1;
    		theta2=temp2;
    	}
    	BufferedReader file1 = new BufferedReader(new FileReader(new File("F:/QQ文档/591195690/FileRecv/学习/testset.txt")));
    	String rowTest;
    	String[] testSet=null;
    	int iTest=0;
    	int[][] testx=new int[1000][1000];
    	int[] testy=new int[1000];
    	while ((rowTest=file1.readLine())!=null){
    		testSet = rowTest.split(",");
        	for (int j=1;j<3;j++){
        		testx[iTest][j]=Integer.parseInt(testSet[j-1]);
        		}
        	testy[iTest]=Integer.parseInt(testSet[2]);
        	iTest++;
    	}
    	iTest--;
//    	调用方法normalize 使x正则化并加上一列x0=1
    	double[][] testx_ = normal.normalize(iTest,testx);
//    	求出代价函数的值
    	HelloWorld cost =new HelloWorld();
    	double j = cost.showJ(iTest, testy, theta0, theta1, theta2, testx_);
    	System.out.println("代价函数的值为:"+j);
    	System.out.println("theta0="+theta0+" theta1="+theta1+" theta2="+theta2);
    }
    public double showJ(int row,int[] y,double theta0,double theta1,double theta2,double[][] test){	//a表示h(x),求出jTheta
    	double jTheta=0;//代价函数
    	double hx;//预测值
    	for (int i=0;i<row;i++){
    		hx=theta0+theta1*test[i][1]+theta2*test[i][2];
    		jTheta+=(hx-y[i])*(hx-y[i]);
    	}
    	jTheta/=70;
    	return jTheta;
    }
    public double derivate(double theta0,double theta1,double theta2,int row,int num, int[] trany,double[][] tranx_){	//求导
    	double der=0;
    	for (int i=0;i<row;i++){
    		double a=theta0*tranx_[i][0]+theta1*tranx_[i][1]+theta2*tranx_[i][2];
    		der+=(a-trany[i])*tranx_[i][num];
    	}
    	der/=row;
    	return der;
    }
 	public double[][] normalize (int row,int[][] setx){		//是x正则化
    	double standDev1=0,standDev2=0;//x1、x2的标准差
    	double u1,u2;//x1、x2的平均值
    	int sum1=0,sum2=0;//x1、x2的总值
    	for (int i=0; i<row; i++){
    		sum1+=setx[i][0];
    		sum2+=setx[i][1];
    	}
    	u1=sum1/35;
    	u2=sum2/35;
    	for (int i=0;i<row;i++){
    		standDev1+=(setx[i][0]-u1)*(setx[i][0]-u1);
    		standDev2+=(setx[i][1]-u2)*(setx[i][1]-u2);
    	}
    	standDev1=Math.sqrt(standDev1/row);
    	standDev2=Math.sqrt(standDev2/row);
    	double[][] setx_=new double[row][3];
    	for (int i=0;i<row;i++){
    		setx_[i][0]=1;
    		setx_[i][1]=(setx[i][0]-u1)/standDev1;
    		setx_[i][2]=(setx[i][1]-u2)/standDev2;	
    	}
    	return setx_;
    }
}